# Welcome to the Code Club Repository

## Versions

Please, go to your year in the different branches. The documentation for each year is located:

- [2021](https://gitlab.com/fablabbcn-projects/learning/code-club/-/tree/2021) or [The resurrected web](https://gitlab.com/fablabbcn-projects/learning/the-resurrected-web)
- [2020](http://academany.fabcloud.io/fabacademy/2020/labs/barcelona/site/local/#material/extras/codeclub/intropython/)
- [2019](http://fab.academany.org/2019/labs/barcelona/local/clubs/codeclub/intropython/)

## Contributing

Please, open PR or issues if something doesn't work. Be polite.
